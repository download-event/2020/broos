# Download Hackathon 2020 team broOS
**link of the site of the project:** https://beeople.000webhostapp.com/view/project.html

**link of the drive folder with documentation:** https://drive.google.com/drive/folders/1s24NLctmTnRSXhBN5CClhNCGXW-71Ng-?usp=sharing

## beeOPLE
beeOPLE is a web application designed for a simple and fast comunication above the network with people from all the world!!!. It's the first social network inspired from natural enviroment; in particular the world of bees. We think that bees phenomenon is very interesting because it shows us the particular system of comunications used in bees's hive to trasmit different signals.To do this, bees use a lot of different dance to comunicate. So, like the bees dance to comunicate, we want you to comunicate to other people with beeOPLE for telling at your "hive" some problems or places which other people will see!

### how does it work?
After registration you will be brought in you profile page. Here your can check you datas.
The honey is the review that other bees gave to you when you segnalate a flower, do more "honey points" and grow up.
You start as a pupe, just a "bee child"; than become a worker bee, help in your hive, collect honey and become the queen.

### how to download and run it:

1. Download the repositories from gitlab
2. Use xampp to load the db click on import and select the sql in the database root
3. put the beeOPLE repositories in htdocs in the xampp directory
4. Start with Apache and you will see the page on localhost:xxxx/beeOPLE/view/ 

> if it doesen't work use the link: https://beeople.000webhostapp.com/view/project.html

> Sorry, the site is a beta so something is not in function corectly, like the chat which is only the demostration of the real one...