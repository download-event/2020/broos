-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Ott 01, 2020 alle 14:59
-- Versione del server: 10.3.16-MariaDB
-- Versione PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id15005412_beeople`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `ambiental_post`
--

CREATE TABLE `ambiental_post` (
  `id` int(11) NOT NULL,
  `position` varchar(100) NOT NULL,
  `flower` varchar(100) NOT NULL,
  `content` varchar(100) NOT NULL,
  `likes` int(11) NOT NULL,
  `dislike` int(11) NOT NULL,
  `author_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `email` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cognome` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`email`, `nome`, `cognome`, `username`, `password`, `city`, `role`) VALUES
('amefog88@gmail.com', 'amerigo', 'capelli', 'cap3', 'password03', 'bergamo', 0),
('averdi444@gmail.com', 'alessandro', 'verdi', 'super', '12345', 'bergamo', 0),
('ciao00@gmail.com', 'caio', 'senpronio', 'ilsenpi', 'password03', 'milano', 0),
('dariolario@@', 'dario', 'lario', 'dariolario', '12', 'milano', 0),
('gmaggi46@gmail.com', 'Gabriele', 'Maggi', 'GabriMag', '1234567', 'bergamo', 2),
('prova3@gmail.com', 'xxx', 'xxx', 'xxx', 'xxx', 'bergamo', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `ambiental_post`
--
ALTER TABLE `ambiental_post`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `ambiental_post`
--
ALTER TABLE `ambiental_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
